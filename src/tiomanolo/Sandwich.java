/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

/**
 *
 * @author Duoc
 */
public abstract class Sandwich implements ICosto{
    
    private String nombre;
    public final int CANTIDAD_PAN = 1;
    private int cantidadCarne;
    private int precioBase;

    public Sandwich(String nombre, int cantidadCarne) {
        this.nombre = nombre;
        this.cantidadCarne = cantidadCarne;
        setPrecioBase();        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadCarne() {
        return cantidadCarne;
    }

    public void setCantidadCarne(int cantidadCarne) {
        this.cantidadCarne = cantidadCarne;
    }

    public int getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase() {
        this.precioBase = (cantidadCarne * COSTO_GRAMO_CARNE) + (CANTIDAD_PAN * COSTO_UNIDAD_PAN);
    }
    
    public abstract int contabilizar();
    
    public int totalVenta(String dia){
        int totalSandwich = contabilizar();
        int gananciaSandwich = (int)(contabilizar() * PORCENTAJE_GANANCIA);
        int totalVenta =  totalSandwich + gananciaSandwich - descontar(dia);        
        System.out.println(mostrar());        
        System.out.println("Descuento\t\t: " + descontar(dia));
        System.out.println("TOTAL SANDWICH\t\t: " + totalVenta);
        System.out.println("---------------------------------------");
        return totalVenta;
    }
    
    
    
    
}
