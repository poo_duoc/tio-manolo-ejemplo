/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

/**
 *
 * @author Duoc
 */
public class Italiano extends Sandwich{

    private int cantidadPalta;
    private int cantidadTomate;
    private int cantidadMayonesa;

    public Italiano(int cantidadPalta, int cantidadTomate, int cantidadMayonesa, int cantidadCarne) {
        super("Italiano", cantidadCarne);
        this.cantidadPalta = cantidadPalta;
        this.cantidadTomate = cantidadTomate;
        this.cantidadMayonesa = cantidadMayonesa;
    }

    public int getCantidadPalta() {
        return cantidadPalta;
    }

    public void setCantidadPalta(int cantidadPalta) {
        this.cantidadPalta = cantidadPalta;
    }

    public int getCantidadTomate() {
        return cantidadTomate;
    }

    public void setCantidadTomate(int cantidadTomate) {
        this.cantidadTomate = cantidadTomate;
    }

    public int getCantidadMayonesa() {
        return cantidadMayonesa;
    }

    public void setCantidadMayonesa(int cantidadMayonesa) {
        this.cantidadMayonesa = cantidadMayonesa;
    }
    
    

    @Override
    public int contabilizar() {
        int costoPalta = cantidadPalta * COSTO_GRAMO_PALTA;
        int costoTomate = cantidadTomate * COSTO_GRAMO_TOMATE;
        int costoMayo = cantidadMayonesa * COSTO_GRAMO_MAYONESA;
        int total = getPrecioBase() + costoPalta + costoTomate + costoMayo;
        return total;
    }

    @Override
    public int descontar(String dia) {
        if(dia.equals("MIERCOLES")){
            return (int)(contabilizar()*PORCENTAJE_DESCUENTO);
        }else{
            return 0;
        }
    }

    @Override
    public String mostrar() {
         return "Nombre Sandwich\t\t: "+ getNombre()+"\n"+
                "Cantidad Pan\t\t: " + CANTIDAD_PAN + "\n" +
                "Cantidad Carne\t\t: " + getCantidadCarne() + "\n" + 
                "Cantidad Palta\t\t: " + getCantidadPalta()+ "\n" +
                "Cantidad Tomate\t\t: " + getCantidadTomate() + "\n" +
                "Cantidad Mayonesa\t: " + getCantidadMayonesa() ;        
    }
    
    
    
    
}
