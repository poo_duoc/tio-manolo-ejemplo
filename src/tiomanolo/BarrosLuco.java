/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

/**
 *
 * @author Duoc
 */
public class BarrosLuco extends Sandwich{
    
    private int cantidadQueso;

    public BarrosLuco(int cantidadQueso, int cantidadCarne) {
        super("Barros Luco", cantidadCarne);
        this.cantidadQueso = cantidadQueso;
    }

    public int getCantidadQueso() {
        return cantidadQueso;
    }

    public void setCantidadQueso(int cantidadQueso) {
        this.cantidadQueso = cantidadQueso;
    }

    @Override
    public int contabilizar() {
        int costoQueso = cantidadQueso * COSTO_GRAMO_QUESO;
        int total = getPrecioBase() + costoQueso;
        return total;
    }

    @Override
    public int descontar(String dia) {
        if(dia.equals("MIERCOLES")){
            return (int)(contabilizar()*PORCENTAJE_DESCUENTO);
        }else{
            return 0;
        }
    }

    @Override
    public String mostrar() {
        return "Nombre Sandwich\t\t: "+ getNombre()+"\n"+
                "Cantidad Pan\t\t: " + CANTIDAD_PAN + "\n" +
                "Cantidad Carne\t\t: " + getCantidadCarne() + "\n" + 
                "Cantidad Queso\t\t: " + getCantidadQueso();       
    }
    
    
    
}
