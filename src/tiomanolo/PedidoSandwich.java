/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

import java.util.ArrayList;

/**
 *
 * @author Duoc
 */
public class PedidoSandwich {
    
    private int indice;
    private ArrayList<Sandwich> sandwichs;

    public PedidoSandwich() {
        indice = 0;
        sandwichs = new ArrayList<>();
    }
    
    public void agregarSandwich(Sandwich newSandwich){
        sandwichs.add(newSandwich);
    }
    
    public void listarSandwich(String dia){
        System.out.println("-------------- Total Boleta ---------------");
        System.out.println("Item Pedidos\t\t: " + sandwichs.size());
        System.out.println("-------------------------------------------");
        int totalBoleta = 0;
        for(Sandwich aux : sandwichs){            
            totalBoleta += aux.totalVenta(dia);
        }
        System.out.println("Total Boleta\t\t: " + totalBoleta);
    }
    
    public void eliminarSandwich(Sandwich eliminaSandwich){
        int indiceAEliminar = 0, indiceAux = 0;
        int totalMenor = 99999999;
        for(Sandwich aux : sandwichs){
            if(aux.equals(eliminaSandwich)){
                if(totalMenor > aux.contabilizar()){
                    totalMenor = aux.contabilizar();
                    indiceAEliminar = indiceAux;
                }
            }
            indiceAux++;
        }
        if(totalMenor < 99999999){
            sandwichs.remove(indiceAEliminar);
        }
    }
    
    
    
    
    
}
