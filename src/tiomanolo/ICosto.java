/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

/**
 *
 * @author Duoc
 */
public interface ICosto {
    
   int COSTO_UNIDAD_PAN = 200;
   int COSTO_GRAMO_CARNE = 10;
   int COSTO_GRAMO_PALTA = 2;
   int COSTO_GRAMO_TOMATE = 4;
   int COSTO_GRAMO_MAYONESA = 1;
   int COSTO_GRAMO_AJI = 1;
   int COSTO_GRAMO_POROTOVERDE = 2;
   int COSTO_GRAMO_QUESO = 5;
   double PORCENTAJE_GANANCIA = 0.5;
   double PORCENTAJE_DESCUENTO = 0.05;
   
   int descontar(String dia);
   String mostrar();

    
}
