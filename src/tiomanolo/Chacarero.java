/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiomanolo;

/**
 *
 * @author Duoc
 */
public class Chacarero extends Sandwich{

    private int cantidadPorotoVerde;
    private int cantidadTomate;
    private int cantidadMayonesa;
    private int cantidadAji;

    public Chacarero(int cantidadPorotoVerde, int cantidadTomate, int cantidadMayonesa, int cantidadCarne, int cantidadAji) {
        super("Chacarero", cantidadCarne);
        this.cantidadPorotoVerde = cantidadPorotoVerde;
        this.cantidadTomate = cantidadTomate;
        this.cantidadMayonesa = cantidadMayonesa;
        this.cantidadAji = cantidadAji;
    }

    public void setCantidadPorotoVerde(int cantidadPorotoVerde) {
        this.cantidadPorotoVerde = cantidadPorotoVerde;
    }

    public void setCantidadTomate(int cantidadTomate) {
        this.cantidadTomate = cantidadTomate;
    }

    public void setCantidadMayonesa(int cantidadMayonesa) {
        this.cantidadMayonesa = cantidadMayonesa;
    }

    public int getCantidadPorotoVerde() {
        return cantidadPorotoVerde;
    }

    public int getCantidadTomate() {
        return cantidadTomate;
    }

    public int getCantidadMayonesa() {
        return cantidadMayonesa;
    }

    public int getCantidadAji() {
        return cantidadAji;
    }

    public void setCantidadAji(int cantidadAji) {
        this.cantidadAji = cantidadAji;
    }
    
    
    
    @Override
    public int contabilizar() {
        int costoPoroto = cantidadPorotoVerde * COSTO_GRAMO_POROTOVERDE;
        int costoTomate = cantidadTomate * COSTO_GRAMO_TOMATE;
        int costoMayo = cantidadMayonesa * COSTO_GRAMO_MAYONESA;
        int costoAji = cantidadAji + COSTO_GRAMO_AJI;
        int total = getPrecioBase() + costoPoroto + costoTomate + costoMayo + costoAji;
        return total;
    }

    @Override
    public int descontar(String dia) {
        if(dia.equals("LUNES")){
            return (int)(contabilizar()*PORCENTAJE_DESCUENTO);
        }else{
            return 0;
        }
    }

    @Override
    public String mostrar() {
        return "Nombre Sandwich\t\t: "+ getNombre()+"\n"+
                "Cantidad Pan\t\t: " + CANTIDAD_PAN + "\n" +
                "Cantidad Carne\t\t: " + getCantidadCarne() + "\n" +
                "Cantidad Porotos Verdes : " + getCantidadPorotoVerde() + "\n" +
                "Cantidad Tomate\t\t: " + getCantidadTomate() + "\n" +
                "Cantidad Aji\t\t: " + getCantidadAji() + "\n" +
                "Cantidad Mayonesa\t: " + getCantidadMayonesa();                
    }

    
    
    
    
    
    
    
    
}
